FROM centos:centos7

ARG SHUNIT2_VERSION=ba130d69bbff304c0c6a9c5e8ab549ae140d6225
ARG INSTALL_DIR=/opt

WORKDIR ${INSTALL_DIR}

# The current release of shunit2 is too old and doesn't have some features
# that we really need, like `assertContains`. I don't like using master,
# We will lock ourselves to this commit.
ADD https://github.com/kward/shunit2/archive/${SHUNIT2_VERSION}.tar.gz ${INSTALL_DIR}/

RUN tar xzvf ${SHUNIT2_VERSION}.tar.gz && \
    mv ${INSTALL_DIR}/shunit2-${SHUNIT2_VERSION}/ ${INSTALL_DIR}/shunit2 && \
    ln -s /opt/shunit2/shunit2 /usr/local/bin/shunit2 && \
    rm ${SHUNIT2_VERSION}.tar.gz

CMD ["/bin/bash"]

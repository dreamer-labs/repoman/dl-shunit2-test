## [1.1.1](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/compare/v1.1.0...v1.1.1) (2019-11-29)


### Bug Fixes

* add shunit2 to path ([efa7cff](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/commit/efa7cff))

# [1.1.0](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/compare/v1.0.0...v1.1.0) (2019-11-27)


### Features

* updated shunit2 to latest ([04f60d6](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/commit/04f60d6))

# 1.0.0 (2019-11-15)


### Features

* Initial Import ([a4a3b64](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/commit/a4a3b64))
* Initial Import ([d9a0af5](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/commit/d9a0af5))
* removed symlink and version from src dir ([5a4b3d6](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/commit/5a4b3d6))
* removed symlink/version from src directory ([cdb4a12](https://gitlab.com/dreamer-labs/repoman/dl-shunit2-test/commit/cdb4a12))
